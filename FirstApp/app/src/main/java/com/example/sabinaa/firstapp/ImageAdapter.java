package com.example.sabinaa.firstapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by SabinaA on 7/25/2017.
 */

public class ImageAdapter extends BaseAdapter {

    ArrayList<String> images;
    Context context;
    private LayoutInflater inflater;

    public ImageAdapter(Context conte,ArrayList<String> imageList){
        images=imageList;
        context=conte;

    }
    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null){
            inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
            convertView=inflater.inflate(R.layout.custom_layout,null);

        }
        ImageView icon=(ImageView) convertView.findViewById(R.id.images);
        icon.setImageURI(Uri.fromFile(new File(images.get(position))));
        return convertView;
    }


}
