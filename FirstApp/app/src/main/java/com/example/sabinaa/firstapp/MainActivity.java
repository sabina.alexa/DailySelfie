package com.example.sabinaa.firstapp;
import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;


public class MainActivity extends AppCompatActivity {

    final String TAG="tag";
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int MY_PERMISSIONS_ALL=3;
    private Timer timer=new Timer();
    private ImageAdapter adapter;
    private ArrayList<String> paths = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar appMenu=(Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(appMenu);
        SelfieNotificationTask selfieNotificationTask = new SelfieNotificationTask(getApplicationContext());
        timer.schedule(selfieNotificationTask,10000,1500000);
        getFromSdcard();
        GridView imageGrid = (GridView) findViewById(R.id.gridView);
        adapter=new ImageAdapter(getApplicationContext(),paths);
        imageGrid.setAdapter(adapter);

        imageGrid.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent=new Intent(getApplicationContext(),DisplayFullPhotoActivity.class);
                intent.putExtra("imageUri",paths.get(position));
                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }



    void makeRequestForPhoto()
    {
        String imageFileName = new BigInteger(130, new SecureRandom()).toString(32);
        File SDImg = null;

        if (checkForFile())
        {
            SDImg = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Daily Selfie");
        }

        File image = null;
        try
        {
            image = File.createTempFile(imageFileName, ".jpg", SDImg);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        Uri uri = Uri.fromFile(image);
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);//this does not put any data in the intent , only writes the file on that path
        takePhotoIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        takePhotoIntent.putExtra(uri.getPath(),"path");
        startActivityForResult(takePhotoIntent, REQUEST_IMAGE_CAPTURE);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId()==R.id.take_photo){
            item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener()
            {
                @Override
                public boolean onMenuItemClick(MenuItem item)
                {
                    makeRequestForPhoto();
                    return true;
                }
            });
        }
        return false;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        if (requestCode == REQUEST_IMAGE_CAPTURE)
        {
            if (resultCode == RESULT_OK)
            {
                paths.clear();
                getFromSdcard();
                Collections.reverse(paths);
                adapter.notifyDataSetChanged();
            }
        }
    }


    boolean checkForFile()
    {
        File folder = new File(Environment.getExternalStorageDirectory() + "/Daily Selfie");
        boolean success = true;
        if (!folder.exists())
        {
            success = folder.mkdir();
        }
        return success;
    }



    public void getFromSdcard()
    {
        if(getApplicationContext().checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(MainActivity.this, new String[] {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_ALL);
        }
        if(getApplicationContext().checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED)
        {
            File file = new File(Environment.getExternalStorageDirectory() + "/Daily Selfie");
            if (file.isDirectory())
            {
                File[] listFile = file.listFiles();
                for (File aListFile : listFile)
                {
                    paths.add(aListFile.getAbsolutePath());
                }
            }
        }
    }
}
