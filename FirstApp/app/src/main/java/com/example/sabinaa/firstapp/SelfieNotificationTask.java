package com.example.sabinaa.firstapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.TimerTask;

class SelfieNotificationTask extends TimerTask {


    private Context context;

    SelfieNotificationTask(Context appContext){
        context=appContext;
    }

    @Override
    public void run() {
        generateSelfieNotification(context);
    }

    private void generateSelfieNotification(Context context){
        String TAG = "tag";
        try {
            Log.i(TAG, "entered in the generateSelfieNotification method");
            int icon = R.mipmap.black_icon;
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Log.i(TAG, "intialised the notificationManager");
            long when = System.currentTimeMillis();
            Log.i(TAG, "intitilised when with current time milli seconds");
            Notification notification = new Notification(icon, "Time for a selfie", 0);
            Log.i(TAG, "initilised notification instance");
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                    new Intent(context, MainActivity.class), 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(
                    context);
            notification = builder.setContentIntent(contentIntent)
                    .setSmallIcon(icon).setTicker("Daily Selfie").setWhen(0)
                    .setAutoCancel(true).setContentTitle("Daily Selfie")
                    .setContentText("Time for a photo").build();

            notificationManager.notify((int) when, notification);
        }catch(Exception ex)
        {
            Log.i(TAG,ex.getLocalizedMessage());
        }

    }
}
