package com.example.sabinaa.firstapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;

public class DisplayFullPhotoActivity extends AppCompatActivity {

    final String TAG="banana";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_full_photo);
        Intent displayIntent=getIntent();
        String photoUri=displayIntent.getStringExtra("imageUri");
        Uri uri=Uri.fromFile(new File(photoUri));
        ImageView image= (ImageView) findViewById(R.id.fullPhoto);
        image.setImageURI(uri);
    }
}
